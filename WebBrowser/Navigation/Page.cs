﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using WebBrowser.Persistence.History;

namespace WebBrowser.Navigation
{
    public class Page
    {
        /// <summary>
        /// Delegate to update the main window when there are changes to display
        /// </summary>
        public delegate void UpdatePage();
        private UpdatePage updatePage;

        private History globalHistory;
        private List<HistoryDB> navHistory = new List<HistoryDB>();
        private int index = -1;

        public bool CachePages = Properties.Settings.Default.Caching;
        private Dictionary<string, HttpResponse> cache = new Dictionary<string, HttpResponse>();

        /// <summary>
        /// Url in page history that is currently displayed for this page
        /// </summary>
        public string ActiveUrl
        {
            get { return navHistory[index].Url; }
        }

        /// <summary>
        /// True if there are some "previous" pages
        /// </summary>
        public bool CanGoBack
        {
            get { return index > 0; }
        }

        /// <summary>
        /// True if there are some "next" pages
        /// </summary>
        public bool CanGoNext
        {
            get { return index < navHistory.Count-1 ; }
        }

        /// <summary>
        /// Page / Tab of a web browser
        /// Provides methods to navigate to new urls or tab's history
        /// </summary>
        /// <param name="history">global history of the browser</param>
        /// <param name="updatePage">delegate to update main window</param>
        public Page(History history, UpdatePage updatePage)
        {
            globalHistory = history;
            this.updatePage = updatePage;
        }

        /// <summary>
        /// Execute http request for a given url
        /// If CachePages is false, use caching for only 1 page, otherwise cache all pages. 
        /// </summary>
        /// <param name="url">url to fetch</param>
        /// <returns>struct containing the main request/response details</returns>
        public HttpResponse Get(string url)
        {
            if (!cache.ContainsKey(url))
            {
                var request = WebRequest.Create(url);
                request.Method = "GET";

                int statusCode;
                var result = "";

                try
                {
                    using (var response = (HttpWebResponse)request.GetResponse())
                    using (var stream = response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        statusCode = (int)response.StatusCode;
                        result = reader.ReadToEnd();
                    }
                }
                catch (WebException e)
                {
                    var eResponse = (HttpWebResponse)e.Response;
                    if (eResponse != null)
                    {
                        statusCode = (int)eResponse.StatusCode;
                        result = statusCode + " - " + (eResponse).StatusCode;
                    }
                    else
                    {
                        statusCode = -1;
                        result = e.Status.ToString();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    statusCode = -1;
                    result = "An error occured during the request.";
                }

                if (!CachePages) cache.Clear();
                cache.Add(url, new HttpResponse(statusCode, url, result));
            }

            return cache[url];
        }

        /// <summary>
        /// Refresh the active url
        /// Needs to empty the cache for that url first
        /// </summary>
        public void Refresh()
        {
            if (cache.ContainsKey(ActiveUrl)) cache.Remove(ActiveUrl);
            Push(ActiveUrl);
        }

        /// <summary>
        /// Push a url to the page history, also execute the request
        /// If there were "next" pages in history, those pages are removed from page history
        /// </summary>
        /// <param name="url">url to push</param>
        public void Push(string url)
        {
            var item = new HistoryDB() { Url = url };

            if (CanGoNext) navHistory.RemoveRange(++index, navHistory.Count - index);
            else index++;

            navHistory.Add(item);
            globalHistory.Add(item);

            Get(navHistory[index].Url);
            updatePage();
        }

        /// <summary>
        /// Navigate to the previous page (don't change history)
        /// </summary>
        public void Back()
        {
            if (CanGoBack) index--;
            updatePage();
        }

        /// <summary>
        /// Navigate to next page (don't change history)
        /// </summary>
        public void Next()
        {
            if (CanGoNext) index++;
            updatePage();
        }

        /// <summary>
        /// Navigate to the default home page (history push)
        /// </summary>
        public void GoToHomePage() { Push(Properties.Settings.Default.HomePage); }
    }
}
