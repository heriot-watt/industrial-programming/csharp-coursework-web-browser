﻿using System.Collections.Generic;
using WebBrowser.Persistence.History;

namespace WebBrowser.Navigation
{
    public class Tabs
    {
        private Page.UpdatePage updatePage;

        private History history;
        private List<Page> pages = new List<Page>();

        public int pageIndex { get; private set; } = 0;

        /// <summary>
        /// True if the tab can be closed (forbids closing all tabs)
        /// </summary>
        public bool CanClose
        {
            get { return pages.Count > 1; }
        }

        /// <summary>
        /// Current active page / tab
        /// </summary>
        public Page CurrentPage
        {
            get { return pages[pageIndex]; }
        }

        /// <summary>
        /// Tabs control for a web browser
        /// Contains a list of pages / tabs and provides methods to control them
        /// </summary>
        /// <param name="history">global history of the browser</param>
        /// <param name="updatePage">delegate to update main window</param>
        public Tabs(History history, Page.UpdatePage updatePage)
        {
            this.history = history;
            this.updatePage = updatePage;

            pages.Add(new Page(history, updatePage));
        }

        /// <summary>
        /// Select another page / tab
        /// </summary>
        /// <param name="index"></param>
        public void SelectTab(int index)
        {
            if (index >= 0 && index < pages.Count) pageIndex = index;
            updatePage();
        }

        /// <summary>
        /// Create a new page / tab
        /// Becomes the active tab and redirect to home page
        /// </summary>
        public void NewTab()
        {
            pages.Add(new Page(history, updatePage));
            pageIndex = pages.Count - 1;
            CurrentPage.GoToHomePage();
        }

        /// <summary>
        /// Close the current page / tab
        /// </summary>
        public void CloseTab()
        {
            if (CanClose)
            {
                pages.RemoveAt(pageIndex);
                pageIndex = pageIndex == pages.Count ? pageIndex - 1 : pageIndex;
                updatePage();
            }
        }

        /// <summary>
        /// Change the the caching property for all pages / tabs
        /// </summary>
        public void ChangeCaching() {
            Properties.Settings.Default.Caching ^= true;
            Properties.Settings.Default.Save();

            for (int i = 0; i < pages.Count; i++) pages[i].CachePages = Properties.Settings.Default.Caching;
        }
    }
}
