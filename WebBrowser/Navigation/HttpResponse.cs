﻿using System.Text.RegularExpressions;

namespace WebBrowser.Navigation
{
    public struct HttpResponse
    {
        public int Status { get; }
        public string Url { get; }
        public string PageTitle { get; }
        public string PageContent { get; }

        /// <summary>
        /// Struct regrouping page information, used for caching and updating the UI
        /// </summary>
        /// <param name="status">status code of the http response</param>
        /// <param name="url">url from the http request</param>
        /// <param name="pageContent">content from the http response; PageTitle is extracted from that content</param>
        public HttpResponse(int status, string url, string pageContent)
        {
            var regex = new Regex(@"(?<=<title.*>)([\s\S]*)(?=</title>)", RegexOptions.IgnoreCase);

            Status = status;
            Url = url;
            PageContent = pageContent;
            PageTitle = regex.Match(pageContent).Value.Trim();
        }
    }
}
