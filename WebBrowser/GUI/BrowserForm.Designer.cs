﻿namespace WebBrowser.GUI
{
    partial class BrowserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BrowserForm));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.backBtn = new System.Windows.Forms.Button();
            this.imageBtnList = new System.Windows.Forms.ImageList(this.components);
            this.nextBtn = new System.Windows.Forms.Button();
            this.homeBtn = new System.Windows.Forms.Button();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.bookmarksMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.addOrEditBookmarksMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeFromBookmarksMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seeBookmarksMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historyMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.cleanHistoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seeHistoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.settingsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.setHomePageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.useCachingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusCode = new System.Windows.Forms.Label();
            this.refreshBtn = new System.Windows.Forms.Button();
            this.pageUrl = new System.Windows.Forms.TextBox();
            this.tabList = new System.Windows.Forms.ComboBox();
            this.newTabBtn = new System.Windows.Forms.Button();
            this.closeTabBtn = new System.Windows.Forms.Button();
            this.pageContent = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel.SuspendLayout();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.AutoSize = true;
            this.tableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel.ColumnCount = 9;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel.Controls.Add(this.backBtn, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.nextBtn, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.homeBtn, 4, 2);
            this.tableLayoutPanel.Controls.Add(this.menu, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.statusCode, 7, 2);
            this.tableLayoutPanel.Controls.Add(this.refreshBtn, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.pageUrl, 5, 2);
            this.tableLayoutPanel.Controls.Add(this.tabList, 5, 1);
            this.tableLayoutPanel.Controls.Add(this.newTabBtn, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.closeTabBtn, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.pageContent, 1, 3);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 5;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(700, 600);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // backBtn
            // 
            this.backBtn.ImageIndex = 0;
            this.backBtn.ImageList = this.imageBtnList;
            this.backBtn.Location = new System.Drawing.Point(8, 56);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(25, 25);
            this.backBtn.TabIndex = 1;
            this.backBtn.TabStop = false;
            this.backBtn.UseVisualStyleBackColor = true;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // imageBtnList
            // 
            this.imageBtnList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageBtnList.ImageStream")));
            this.imageBtnList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageBtnList.Images.SetKeyName(0, "back.png");
            this.imageBtnList.Images.SetKeyName(1, "next.png");
            this.imageBtnList.Images.SetKeyName(2, "refresh.png");
            this.imageBtnList.Images.SetKeyName(3, "home.png");
            this.imageBtnList.Images.SetKeyName(4, "search.png");
            // 
            // nextBtn
            // 
            this.nextBtn.ImageIndex = 1;
            this.nextBtn.ImageList = this.imageBtnList;
            this.nextBtn.Location = new System.Drawing.Point(39, 56);
            this.nextBtn.Name = "nextBtn";
            this.nextBtn.Size = new System.Drawing.Size(25, 25);
            this.nextBtn.TabIndex = 2;
            this.nextBtn.TabStop = false;
            this.nextBtn.UseVisualStyleBackColor = true;
            this.nextBtn.Click += new System.EventHandler(this.nextBtn_Click);
            // 
            // homeBtn
            // 
            this.homeBtn.ImageIndex = 3;
            this.homeBtn.ImageList = this.imageBtnList;
            this.homeBtn.Location = new System.Drawing.Point(101, 56);
            this.homeBtn.Name = "homeBtn";
            this.homeBtn.Size = new System.Drawing.Size(25, 25);
            this.homeBtn.TabIndex = 3;
            this.homeBtn.TabStop = false;
            this.homeBtn.UseVisualStyleBackColor = true;
            this.homeBtn.Click += new System.EventHandler(this.homeBtn_Click);
            // 
            // menu
            // 
            this.tableLayoutPanel.SetColumnSpan(this.menu, 9);
            this.menu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bookmarksMenu,
            this.historyMenu,
            this.settingsMenu});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(700, 24);
            this.menu.TabIndex = 7;
            this.menu.MenuActivate += new System.EventHandler(this.menu_MenuActivate);
            this.menu.MenuDeactivate += new System.EventHandler(this.menu_MenuDeactivate);
            // 
            // bookmarksMenu
            // 
            this.bookmarksMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addOrEditBookmarksMenuItem,
            this.removeFromBookmarksMenuItem,
            this.seeBookmarksMenuItem});
            this.bookmarksMenu.Name = "bookmarksMenu";
            this.bookmarksMenu.Size = new System.Drawing.Size(78, 20);
            this.bookmarksMenu.Text = "Bookmarks";
            // 
            // addOrEditBookmarksMenuItem
            // 
            this.addOrEditBookmarksMenuItem.Name = "addOrEditBookmarksMenuItem";
            this.addOrEditBookmarksMenuItem.Size = new System.Drawing.Size(208, 22);
            this.addOrEditBookmarksMenuItem.Text = "Add to bookmarks";
            this.addOrEditBookmarksMenuItem.Click += new System.EventHandler(this.addOrEditBookmarksMenuItem_Click);
            // 
            // removeFromBookmarksMenuItem
            // 
            this.removeFromBookmarksMenuItem.Name = "removeFromBookmarksMenuItem";
            this.removeFromBookmarksMenuItem.Size = new System.Drawing.Size(208, 22);
            this.removeFromBookmarksMenuItem.Text = "Remove from bookmarks";
            this.removeFromBookmarksMenuItem.Click += new System.EventHandler(this.removeFromBookmarksMenuItem_Click);
            // 
            // seeBookmarksMenuItem
            // 
            this.seeBookmarksMenuItem.Name = "seeBookmarksMenuItem";
            this.seeBookmarksMenuItem.Size = new System.Drawing.Size(208, 22);
            this.seeBookmarksMenuItem.Text = "See bookmarks";
            this.seeBookmarksMenuItem.Click += new System.EventHandler(this.seeBookmarksMenuItem_Click);
            // 
            // historyMenu
            // 
            this.historyMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cleanHistoryMenuItem,
            this.seeHistoryMenuItem,
            this.toolStripSeparator1});
            this.historyMenu.Name = "historyMenu";
            this.historyMenu.Size = new System.Drawing.Size(57, 20);
            this.historyMenu.Text = "History";
            // 
            // cleanHistoryMenuItem
            // 
            this.cleanHistoryMenuItem.Name = "cleanHistoryMenuItem";
            this.cleanHistoryMenuItem.Size = new System.Drawing.Size(143, 22);
            this.cleanHistoryMenuItem.Text = "Clean history";
            this.cleanHistoryMenuItem.Click += new System.EventHandler(this.cleanHistoryMenuItem_Click);
            // 
            // seeHistoryMenuItem
            // 
            this.seeHistoryMenuItem.Name = "seeHistoryMenuItem";
            this.seeHistoryMenuItem.Size = new System.Drawing.Size(143, 22);
            this.seeHistoryMenuItem.Text = "See history";
            this.seeHistoryMenuItem.Click += new System.EventHandler(this.seeHistoryMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(140, 6);
            // 
            // settingsMenu
            // 
            this.settingsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setHomePageMenuItem,
            this.useCachingToolStripMenuItem});
            this.settingsMenu.Name = "settingsMenu";
            this.settingsMenu.Size = new System.Drawing.Size(61, 20);
            this.settingsMenu.Text = "Settings";
            // 
            // setHomePageMenuItem
            // 
            this.setHomePageMenuItem.Name = "setHomePageMenuItem";
            this.setHomePageMenuItem.Size = new System.Drawing.Size(167, 22);
            this.setHomePageMenuItem.Text = "Set as home page";
            this.setHomePageMenuItem.Click += new System.EventHandler(this.setHomePageMenuItem_Click);
            // 
            // useCachingToolStripMenuItem
            // 
            this.useCachingToolStripMenuItem.Name = "useCachingToolStripMenuItem";
            this.useCachingToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.useCachingToolStripMenuItem.Click += new System.EventHandler(this.useCachingToolStripMenuItem_Click);
            // 
            // statusCode
            // 
            this.statusCode.AutoSize = true;
            this.statusCode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.statusCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusCode.Location = new System.Drawing.Point(622, 56);
            this.statusCode.Margin = new System.Windows.Forms.Padding(3);
            this.statusCode.Name = "statusCode";
            this.statusCode.Size = new System.Drawing.Size(69, 25);
            this.statusCode.TabIndex = 8;
            this.statusCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // refreshBtn
            // 
            this.refreshBtn.ImageIndex = 2;
            this.refreshBtn.ImageList = this.imageBtnList;
            this.refreshBtn.Location = new System.Drawing.Point(70, 56);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(25, 25);
            this.refreshBtn.TabIndex = 9;
            this.refreshBtn.TabStop = false;
            this.refreshBtn.UseVisualStyleBackColor = true;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // pageUrl
            // 
            this.pageUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.SetColumnSpan(this.pageUrl, 2);
            this.pageUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageUrl.Location = new System.Drawing.Point(132, 56);
            this.pageUrl.Name = "pageUrl";
            this.pageUrl.Size = new System.Drawing.Size(484, 23);
            this.pageUrl.TabIndex = 4;
            this.pageUrl.TabStop = false;
            this.pageUrl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pageUrl_KeyDown);
            // 
            // tabList
            // 
            this.tabList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tabList.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.tabList.Items.AddRange(new object[] {
            ""});
            this.tabList.Location = new System.Drawing.Point(132, 28);
            this.tabList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 3);
            this.tabList.MaxDropDownItems = 10;
            this.tabList.Name = "tabList";
            this.tabList.Size = new System.Drawing.Size(239, 21);
            this.tabList.TabIndex = 11;
            this.tabList.TabStop = false;
            this.tabList.SelectionChangeCommitted += new System.EventHandler(this.tabList_SelectionChangeCommitted);
            // 
            // newTabBtn
            // 
            this.tableLayoutPanel.SetColumnSpan(this.newTabBtn, 2);
            this.newTabBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newTabBtn.Location = new System.Drawing.Point(8, 27);
            this.newTabBtn.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.newTabBtn.Name = "newTabBtn";
            this.newTabBtn.Size = new System.Drawing.Size(59, 23);
            this.newTabBtn.TabIndex = 12;
            this.newTabBtn.TabStop = false;
            this.newTabBtn.Text = "New tab";
            this.newTabBtn.UseVisualStyleBackColor = true;
            this.newTabBtn.Click += new System.EventHandler(this.newTabBtn_Click);
            // 
            // closeTabBtn
            // 
            this.tableLayoutPanel.SetColumnSpan(this.closeTabBtn, 2);
            this.closeTabBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.closeTabBtn.Location = new System.Drawing.Point(67, 27);
            this.closeTabBtn.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.closeTabBtn.Name = "closeTabBtn";
            this.closeTabBtn.Size = new System.Drawing.Size(59, 23);
            this.closeTabBtn.TabIndex = 13;
            this.closeTabBtn.TabStop = false;
            this.closeTabBtn.Text = "Close tab";
            this.closeTabBtn.UseVisualStyleBackColor = true;
            this.closeTabBtn.Click += new System.EventHandler(this.closeTabBtn_Click);
            // 
            // pageContent
            // 
            this.pageContent.AcceptsReturn = true;
            this.pageContent.AcceptsTab = true;
            this.tableLayoutPanel.SetColumnSpan(this.pageContent, 7);
            this.pageContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageContent.Location = new System.Drawing.Point(8, 87);
            this.pageContent.Multiline = true;
            this.pageContent.Name = "pageContent";
            this.pageContent.ReadOnly = true;
            this.pageContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.pageContent.Size = new System.Drawing.Size(683, 505);
            this.pageContent.TabIndex = 14;
            this.pageContent.TabStop = false;
            // 
            // BrowserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 600);
            this.Controls.Add(this.tableLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BrowserForm";
            this.Text = "Browser";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BrowserForm_KeyDown);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TextBox pageUrl;
        private System.Windows.Forms.ImageList imageBtnList;
        private System.Windows.Forms.Button backBtn;
        private System.Windows.Forms.Button nextBtn;
        private System.Windows.Forms.Button homeBtn;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem bookmarksMenu;
        private System.Windows.Forms.ToolStripMenuItem addOrEditBookmarksMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeFromBookmarksMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historyMenu;
        private System.Windows.Forms.ToolStripMenuItem seeBookmarksMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cleanHistoryMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seeHistoryMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsMenu;
        private System.Windows.Forms.ToolStripMenuItem setHomePageMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label statusCode;
        private System.Windows.Forms.Button refreshBtn;
        private System.Windows.Forms.ComboBox tabList;
        private System.Windows.Forms.Button newTabBtn;
        private System.Windows.Forms.Button closeTabBtn;
        private System.Windows.Forms.TextBox pageContent;
        private System.Windows.Forms.ToolStripMenuItem useCachingToolStripMenuItem;
    }
}

