﻿using System;
using System.Windows.Forms;

using WebBrowser.Persistence.History;

namespace WebBrowser.GUI
{
    public partial class HistoryForm : Form
    {
        public delegate void GoToPage(string url);
        private GoToPage goToPage;

        /// <summary>
        /// Creates the history display form
        /// </summary>
        /// <param name="goToPage">delegate to use when a history item is clicked</param>
        public HistoryForm(GoToPage goToPage)
        {
            InitializeComponent();
            this.goToPage = goToPage;
        }

        /// <summary>
        /// Append each history item to the list display
        /// </summary>
        /// <param name="bookmarks">list of bookmark</param>
        public void UpdateHistory(History history)
        {
            foreach (var item in history)
            {
                var listItem = new ListViewItem(new string[] {
                    item.Time.ToShortDateString(),
                    item.Time.ToShortTimeString(),
                    item.Url
                });
                listView.Items.Insert(0, listItem);
            }
        }
        
        /// <summary>
        /// Disable closing of the window, hides it instead
        /// Clear the history display list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HistoryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                listView.Items.Clear();
                Hide();
            }
        }

        /// <summary>
        /// Let the browser navigate to the selected url
        /// </summary>
        /// <param name="sender">listview object</param>
        /// <param name="e">event args</param>
        private void listView_ItemActivate(object sender, EventArgs e)
        {
            var url = ((ListView)sender).SelectedItems[0].SubItems[2].Text;
            goToPage(url);
            Close();
        }
    }
}
