﻿using System;
using System.Windows.Forms;

using WebBrowser.Navigation;
using WebBrowser.Persistence.History;
using WebBrowser.Persistence.Bookmarks;

namespace WebBrowser.GUI
{
    public partial class BrowserForm : Form
    {
        private HistoryForm historyForm;
        private History history;

        private BookmarksForm bookmarksForm;
        private BookmarkNameForm bookmarkNameForm;
        private Bookmarks bookmarks;

        private Tabs tabs;

        /// <summary>
        /// Main browser window
        /// </summary>
        public BrowserForm()
        {
            InitializeComponent();

            history = new History();
            bookmarks = new Bookmarks();

            tabs = new Tabs(history, UpdatePage);
            tabs.CurrentPage.GoToHomePage();

            tabList.SelectedIndex = 0;
        }

        /// <summary>
        /// Method updating the UI for the web browser
        /// Used as delegate
        /// </summary>
        private void UpdatePage()
        {
            var res = tabs.CurrentPage.Get(tabs.CurrentPage.ActiveUrl);

            pageUrl.Text = tabs.CurrentPage.ActiveUrl;
            statusCode.Text = "Status Code\n" + ((res.Status != -1) ? res.Status.ToString() : "-");
            pageContent.Text = res.PageContent;
            
            tabList.Items[tabs.pageIndex] = res.PageTitle != "" ? res.PageTitle : pageUrl.Text;

            backBtn.Enabled = tabs.CurrentPage.CanGoBack;
            nextBtn.Enabled = tabs.CurrentPage.CanGoNext;
            newTabBtn.Enabled = tabList.Items.Count < 10;
            closeTabBtn.Enabled = tabs.CanClose;
        }

        ///
        /// Tabs handling
        ///

        private void newTabBtn_Click(object sender, EventArgs e)
        {
            tabList.Items.Add("");
            tabs.NewTab();
            tabList.SelectedIndex = tabs.pageIndex;
        }
        private void closeTabBtn_Click(object sender, EventArgs e)
        {
            if (tabs.CanClose)
            {
                tabList.Items.RemoveAt(tabs.pageIndex);
                tabs.CloseTab();
                tabList.SelectedIndex = tabs.pageIndex;
            }
        }
        private void tabList_SelectionChangeCommitted(object sender, EventArgs e) { tabs.SelectTab(((ComboBox)sender).SelectedIndex); }

        ///
        /// New window forms handling
        ///

        private void OpenHistory()
        {
            if (historyForm == null) historyForm = new HistoryForm(tabs.CurrentPage.Push);
            historyForm.UpdateHistory(history);
            historyForm.ShowDialog(this);
        }

        private void OpenBookmarks()
        {
            if (bookmarksForm == null) bookmarksForm = new BookmarksForm(tabs.CurrentPage.Push);
            bookmarksForm.UpdateBookmarks(bookmarks);
            bookmarksForm.ShowDialog(this);
        }

        private void OpenBookmarkName()
        {
            var index = bookmarks.FindIndex(pageUrl.Text);
            if (bookmarkNameForm == null) bookmarkNameForm = new BookmarkNameForm();

            bookmarkNameForm.UpdateName(index != -1 ? bookmarks[index].Name : "", delegate (string name) {
                if (index != -1) bookmarks.Edit(pageUrl.Text, name);
                else bookmarks.Add(new BookmarkDB() { Url = pageUrl.Text, Name = name });
            });

            bookmarkNameForm.ShowDialog(this);
        }

        ///
        /// Handle buttons click and keyboard shortcuts
        ///

        private void backBtn_Click(object sender, EventArgs e) { tabs.CurrentPage.Back(); }
        private void nextBtn_Click(object sender, EventArgs e) { tabs.CurrentPage.Next(); }
        private void homeBtn_Click(object sender, EventArgs e) { tabs.CurrentPage.GoToHomePage(); }
        private void refreshBtn_Click(object sender, EventArgs e) { tabs.CurrentPage.Refresh(); }
        private void pageUrl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                tabs.CurrentPage.Push(pageUrl.Text);
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void BrowserForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5) tabs.CurrentPage.Refresh();
            else if (e.Control && e.KeyCode == Keys.H) OpenHistory();
            else if (e.Control && e.KeyCode == Keys.B) OpenBookmarks();
        }

        ///
        /// Handle browser menu actions
        ///

        private void historyItem_Click(object sender, EventArgs e) { tabs.CurrentPage.Push(((ToolStripMenuItem)sender).Text); }

        /// <summary>
        /// Update the menu display every time it is activated
        /// </summary>
        /// <param name="sender">menu object</param>
        /// <param name="e">event args</param>
        private void menu_MenuActivate(object sender, EventArgs e)
        {
            var bookmarked = bookmarks.Exists(pageUrl.Text);

            // Update the history summary list
            foreach (var item in history.Summary())
            {
                var menuItem = new ToolStripMenuItem() { Text = item.Url, Name = "historyItem" };
                menuItem.Click += new EventHandler(historyItem_Click);
                historyMenu.DropDownItems.Add(menuItem);
            }
            
            useCachingToolStripMenuItem.Text = tabs.CurrentPage.CachePages ? "Disable caching" : "Enable caching";
            addOrEditBookmarksMenuItem.Text = bookmarked ? "Edit bookmark" : "Add to bookmarks";
            removeFromBookmarksMenuItem.Enabled = bookmarked;
        }

        /// <summary>
        /// Reset some items from the menu when it desactivates
        /// </summary>
        /// <param name="sender">menu object</param>
        /// <param name="e">event args</param>
        private void menu_MenuDeactivate(object sender, EventArgs e)
        {
            foreach (var item in historyMenu.DropDownItems.Find("historyItem", false))
                historyMenu.DropDownItems.Remove(item);
        }

        private void setHomePageMenuItem_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.HomePage = pageUrl.Text;
            Properties.Settings.Default.Save();
        }

        private void cleanHistoryMenuItem_Click(object sender, EventArgs e) { history.Clean(); }
        private void seeHistoryMenuItem_Click(object sender, EventArgs e) { OpenHistory(); }

        private void addOrEditBookmarksMenuItem_Click(object sender, EventArgs e) { OpenBookmarkName(); }
        private void removeFromBookmarksMenuItem_Click(object sender, EventArgs e) { bookmarks.Remove(pageUrl.Text); }
        private void seeBookmarksMenuItem_Click(object sender, EventArgs e) { OpenBookmarks(); }

        private void useCachingToolStripMenuItem_Click(object sender, EventArgs e) { tabs.ChangeCaching(); }
    }
}
