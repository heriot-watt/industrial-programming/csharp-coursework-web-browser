﻿using System;
using System.Windows.Forms;

using WebBrowser.Persistence.Bookmarks;

namespace WebBrowser.GUI
{
    public partial class BookmarksForm : Form
    {
        public delegate void GoToPage(string url);
        private GoToPage goToPage;

        /// <summary>
        /// Creates the bookmarks display form
        /// </summary>
        /// <param name="goToPage">delegate to use when a bookmark is clicked</param>
        public BookmarksForm(GoToPage goToPage)
        {
            InitializeComponent();
            this.goToPage = goToPage;
        }

        /// <summary>
        /// Append each bookmark to the list display
        /// </summary>
        /// <param name="bookmarks">list of bookmark</param>
        public void UpdateBookmarks(Bookmarks bookmarks)
        {
            foreach (var item in bookmarks)
            {
                var listItem = new ListViewItem(new string[] { item.Name, item.Url });
                listView.Items.Add(listItem);
            }
        }

        /// <summary>
        /// Disable closing of the window, hides it instead
        /// Clear the bookmarks display list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BookmarksForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                listView.Items.Clear();
                Hide();
            }
        }

        /// <summary>
        /// Let the browser navigate to the selected url
        /// </summary>
        /// <param name="sender">listview object</param>
        /// <param name="e">event args</param>
        private void listView_ItemActivate(object sender, EventArgs e)
        {
            var url = ((ListView)sender).SelectedItems[0].SubItems[1].Text;
            goToPage(url);
            Close();
        }
    }
}
