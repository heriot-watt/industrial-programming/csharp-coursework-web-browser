﻿using System;
using System.Windows.Forms;

namespace WebBrowser.GUI
{
    public partial class BookmarkNameForm : Form
    {
        public delegate void Save(string name);
        private Save save;

        /// <summary>
        /// Creates the form for editing a bookmark's name
        /// </summary>
        public BookmarkNameForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Update the form with the bookmarks name and the delegate to save edit
        /// </summary>
        /// <param name="name">current name of the bookmark</param>
        /// <param name="save">delegate to save the name modification</param>
        public void UpdateName(string name, Save save) {
            textBox.Text = name;
            this.save = save;
        }

        /// <summary>
        /// Edit the bookmark and close the window
        /// </summary>
        private void Edit()
        {
            if (textBox.Text != "" && save != null)
            {
                save(textBox.Text);
                Close();
            }
        }

        /// <summary>
        /// Handle the form validation
        /// </summary>

        private void okBtn_Click(object sender, EventArgs e) { Edit(); }
        private void textBox_KeyDown(object sender, KeyEventArgs e) { if (e.KeyCode == Keys.Enter) Edit(); }

        /// <summary>
        /// Disable closing of the window, hides it instead
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BookmarkNameForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }
    }
}
