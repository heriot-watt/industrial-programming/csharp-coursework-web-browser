﻿using System;
using System.Collections.Generic;

namespace WebBrowser.Persistence.History
{
    public class History
    {
        private List<HistoryDB> history;
        private HistoryDBConnector dbConn;

        public int Count { get { return history.Count; } }

        /// <summary>
        /// Access point to the browser history
        /// Provides methods for editing the history and update the persisted data
        /// </summary>
        public History()
        {
            dbConn = new HistoryDBConnector();
            Sync();
        }

        /// <summary>
        /// Synchronize history from database to the runtime instance
        /// </summary>
        private void Sync() { history = dbConn.HistoryList; }

        /// <summary>
        /// Append an item to the history in both runtime instance and database
        /// </summary>
        /// <param name="item">item to insert</param>
        public void Add(HistoryDB item)
        {
            history.Add(item);
            dbConn.Add(item);
        }

        /// <summary>
        /// Delete all history data in both runtime instance and database
        /// </summary>
        public void Clean() {
            dbConn.Clean();
            Sync();
        }

        /// <summary>
        /// Gets the last history items (10 items)
        /// </summary>
        /// <returns>The 10 last history item, ordered by the most recent</returns>
        public List<HistoryDB> Summary()
        {
            var list = new List<HistoryDB>();

            for (int i = 0; ((history.Count - 1) - i) >= 0 && i < 10; i++)
                list.Add(history[(history.Count - 1) - i]);

            return list;
        }

        /// <summary>
        /// Indexer for the History class (getter only)
        /// </summary>
        /// <param name="index">index of item to retreive</param>
        /// <returns>The item from the given index</returns>
        public HistoryDB this[int index]
        {
            get
            {
                if (index >= 0 && index < history.Count) return history[index];
                else throw new IndexOutOfRangeException("History index out of bound");
            }
        }

        /// <summary>
        /// Method necessary for using the class instance in a foreach loop
        /// </summary>
        /// <returns>A IEnumerator for the class History</returns>
        public IEnumerator<HistoryDB> GetEnumerator() => history.GetEnumerator();
    }
}
