﻿using SQLite;
using System.Collections.Generic;

namespace WebBrowser.Persistence.History
{
    class HistoryDBConnector
    {
        private readonly string path = "history.db";
        private SQLiteConnection db;

        /// <summary>
        /// Connector for persisted history
        /// Give few methods for syncing data between runtime instance and database
        /// </summary>
        public HistoryDBConnector()
        {
            db = new SQLiteConnection(path);
            db.CreateTable<HistoryDB>();
        }

        /// <summary>
        /// Gets persisted history
        /// </summary>
        public List<HistoryDB> HistoryList
        {
            get { return db.Table<HistoryDB>().ToList(); }
        }

        /// <summary>
        /// Insert history item in database
        /// </summary>
        /// <param name="item">item to insert</param>
        public void Add(HistoryDB item) { db.Insert(item); }

        /// <summary>
        /// Delete all history data from the database
        /// </summary>
        public void Clean() { db.DeleteAll<HistoryDB>(); }
    }
}
