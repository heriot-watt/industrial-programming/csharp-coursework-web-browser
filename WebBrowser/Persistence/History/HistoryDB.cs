﻿using SQLite;
using System;

namespace WebBrowser.Persistence.History
{
    /// <summary>
    /// Database representation of the history table
    /// </summary>
    public class HistoryDB
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public DateTime Time { get; set; } = DateTime.Now;
        public string Url { get; set; }
    }
}
