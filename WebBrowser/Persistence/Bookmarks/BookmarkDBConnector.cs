﻿using SQLite;
using System.Collections.Generic;

namespace WebBrowser.Persistence.Bookmarks
{
    public class BookmarkDBConnector
    {
        private readonly string path = "bookmarks.db";
        private SQLiteConnection db;

        /// <summary>
        /// Connector for persisted bookmarks
        /// Give few methods for syncing data between runtime instance and database
        /// </summary>
        public BookmarkDBConnector()
        {
            db = new SQLiteConnection(path);
            db.CreateTable<BookmarkDB>();
        }
        
        /// <summary>
        /// Gets persisted bookmarks
        /// </summary>
        public List<BookmarkDB> BookmarksList
        {
            get { return db.Table<BookmarkDB>().ToList(); }
        }

        /// <summary>
        /// Find and return a bookmark from the database
        /// </summary>
        /// <param name="url">url to search for in database</param>
        /// <returns>Found bookmark; null otherwise</returns>
        private BookmarkDB Find(string url) => db.Table<BookmarkDB>().Where(elt => elt.Url == url).FirstOrDefault();

        /// <summary>
        /// Insert history item in database
        /// </summary>
        /// <param name="item">item to insert</param>
        public void Add(BookmarkDB item) { if (Find(item.Url) == null) db.Insert(item); }

        /// <summary>
        /// Edit a bookmark in database
        /// </summary>
        /// <param name="item">bookmark item to update</param>
        public void Edit(BookmarkDB item) { if (Find(item.Url) != null) db.Update(item); }

        /// <summary>
        /// Delete a given url from persisted bookmarks
        /// </summary>
        /// <param name="url">url to remove from the database</param>
        public void Remove(string url)
        {
            var item = Find(url);
            if (item != null) db.Delete<BookmarkDB>(item.ID);
        }
    }
}
