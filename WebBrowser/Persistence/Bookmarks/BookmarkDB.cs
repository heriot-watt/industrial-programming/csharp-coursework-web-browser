﻿using SQLite;

namespace WebBrowser.Persistence.Bookmarks
{
    /// <summary>
    /// Database representation of the bookmarks table
    /// </summary>
    public class BookmarkDB
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
