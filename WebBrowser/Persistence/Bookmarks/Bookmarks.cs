﻿using System;
using System.Collections.Generic;

namespace WebBrowser.Persistence.Bookmarks
{
    public class Bookmarks
    {
        private List<BookmarkDB> bookmarks;
        private BookmarkDBConnector dbConn;

        public int Count { get { return bookmarks.Count; } }

        /// <summary>
        /// Access point to the browser bookmarks
        /// Provides methods for editing the bookmarks and update the persisted data
        /// </summary>
        public Bookmarks()
        {
            dbConn = new BookmarkDBConnector();
            Sync();
        }
        
        /// <summary>
        /// Synchronize history from database to the runtime instance
        /// </summary>
        private void Sync() { bookmarks = dbConn.BookmarksList; }

        /// <summary>
        /// Finds the index of a bookmark given its url
        /// </summary>
        /// <param name="url">url to match in the bookmarks list</param>
        /// <returns>index of the matched bookmark; -1 otherwise</returns>
        public int FindIndex(string url) => bookmarks.FindIndex(delegate (BookmarkDB item) { return item.Url == url; });

        /// <summary>
        /// Returns true if the url already exists in the bookmarks list
        /// </summary>
        /// <param name="url">url to match</param>
        /// <returns>true if the url is bookmarked, false otherwise</returns>
        public bool Exists(string url) => FindIndex(url) != -1;

        /// <summary>
        /// Append an item to the bookmarks in both runtime instance and database
        /// </summary>
        /// <param name="item">item to insert</param>
        public void Add(BookmarkDB item)
        {
            if (!Exists(item.Url))
            {
                bookmarks.Add(item);
                dbConn.Add(item);
            }
        }

        /// <summary>
        /// Edit a bookmark; update both runtime instance and database
        /// </summary>
        /// <param name="url">url as identifier of the bookmark</param>
        /// <param name="name">new name for the bookmark</param>
        public void Edit(string url, string name)
        {
            var index = FindIndex(url);
            if (index != -1)
            {
                bookmarks[index].Name = name;
                dbConn.Edit(bookmarks[index]);
            }
        }

        /// <summary>
        /// Delete a specific bookmark from both runtime instance and database
        /// </summary>
        /// <param name="url">url from the bookmark to match</param>
        public void Remove(string url)
        {
            var index = FindIndex(url);
            if (index != -1)
            {
                dbConn.Remove(url);
                bookmarks.RemoveAt(index);
            }
        }
        
        /// <summary>
        /// Indexer for the Bookmarks class (getter only)
        /// </summary>
        /// <param name="index">index of item to retreive</param>
        /// <returns>The item from the given index</returns>
        public BookmarkDB this[int index]
        {
            get
            {
                if (index >= 0 && index < bookmarks.Count) return bookmarks[index];
                else throw new IndexOutOfRangeException("Bookmarks index out of bound");
            }
        }

        /// <summary>
        /// Method necessary for using the class instance in a foreach loop
        /// </summary>
        /// <returns>A IEnumerator for the class Bookmarks</returns>
        public IEnumerator<BookmarkDB> GetEnumerator() => bookmarks.GetEnumerator();
    }
}
